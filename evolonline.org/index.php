<?php
/**
 * @package     Joomla.Administrator
 * @subpackage  Templates.protostar
 *
 * @copyright   Copyright (C) 2005 - 2013 Open Source Matters, Inc. All rights reserved.
 * @license     GNU General Public License version 2 or later; see LICENSE.txt
 */

defined('_JEXEC') or die;

// Getting params from template
$params = JFactory::getApplication()->getTemplate(true)->params;

$app = JFactory::getApplication();
$doc = JFactory::getDocument();
$this->language = $doc->language;
$this->direction = $doc->direction;

// Detecting Active Variables
$option   = $app->input->getCmd('option', '');
$view     = $app->input->getCmd('view', '');
$layout   = $app->input->getCmd('layout', '');
$task     = $app->input->getCmd('task', '');
$itemid   = $app->input->getCmd('Itemid', '');
$sitename = $app->getCfg('sitename');

if($task == "edit" || $layout == "form" )
{
  $fullWidth = 1;
}
else
{
  $fullWidth = 0;
}

// Add JavaScript Frameworks
JHtml::_('bootstrap.framework');

// Add Stylesheets
$doc->addStyleSheet('templates/'.$this->template.'/css/template.css');

// Add JavaScripts
$doc->addScript('templates/' . $this->template . '/js/main.js', 'text/javascript');

// Load optional rtl Bootstrap css and Bootstrap bugfixes
JHtmlBootstrap::loadCss($includeMaincss = false, $this->direction);

// Add current user information
$user = JFactory::getUser();

// Adjusting content width
if ($this->countModules('right-sidebar') && $this->countModules('left-sidebar'))
{
  $span = "span6";
}
elseif ($this->countModules('right-sidebar') && !$this->countModules('left-sidebar'))
{
  $span = "span9";
}
elseif (!$this->countModules('right-sidebar') && $this->countModules('left-sidebar'))
{
  $span = "span9";
}
else
{
  $span = "span12";
}

// Logo file or site title param
if ($this->params->get('logoFile'))
{
  $logo = '<img src="'. JURI::root() . $this->params->get('logoFile') .'" alt="'. $sitename .'" />';
}
elseif ($this->params->get('sitetitle'))
{
  $logo = '<span class="site-title" title="'. $sitename .'">'. htmlspecialchars($this->params->get('sitetitle')) .'</span>';
}
else
{
  $logo = '<span class="site-title" title="'. $sitename .'">'. $sitename .'</span>';
}
?>
<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $this->language; ?>" lang="<?php echo $this->language; ?>" dir="<?php echo $this->direction; ?>">
<head>
  <meta name="viewport" content="width=device-width, initial-scale=1.0" />
  <jdoc:include type="head" />
  <?php
  // Use of Google Font
  if ($this->params->get('googleFont'))
  {
  ?>
    <link href='http://fonts.googleapis.com/css?family=<?php echo $this->params->get('googleFontName');?>' rel='stylesheet' type='text/css' />
    <style type="text/css">
      h1,h2,h3,h4,h5,h6,.site-title{
        font-family: '<?php echo str_replace('+', ' ', $this->params->get('googleFontName'));?>', sans-serif;
      }
    </style>
  <?php
  }
  ?>
  <?php
  // Template color
  if ($this->params->get('templateColor'))
  {
  ?>
  <style type="text/css">
    body.site
    {
      /*border-top: 3px solid <?php echo $this->params->get('templateColor');?>;*/
      background-color: <?php echo $this->params->get('templateBackgroundColor');?>
    }
    a
    {
      color: <?php echo $this->params->get('templateColor');?>;
    }
    .navbar-inner, .nav-list > .active > a, .nav-list > .active > a:hover, .dropdown-menu li > a:hover, .dropdown-menu .active > a, .dropdown-menu .active > a:hover, .nav-pills > .active > a, .nav-pills > .active > a:hover,
    .btn-primary
    {
      background: <?php echo $this->params->get('templateColor');?>;
    }
    .navbar-inner
    {
      -moz-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
      -webkit-box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
      box-shadow: 0 1px 3px rgba(0, 0, 0, .25), inset 0 -1px 0 rgba(0, 0, 0, .1), inset 0 30px 10px rgba(0, 0, 0, .2);
    }
  </style>
  <?php
  }
  ?>
  <!--[if lt IE 9]>
    <script src="<?php echo $this->baseurl ?>/media/jui/js/html5.js"></script>
  <![endif]-->
</head>

<body class="site <?php echo $option
  . ' view-' . $view
  . ($layout ? ' layout-' . $layout : ' no-layout')
  . ($task ? ' task-' . $task : ' no-task')
  . ($itemid ? ' itemid-' . $itemid : '')
  . ($params->get('fluidContainer') ? ' fluid' : '');
?>">

  <!-- Body -->
  <div class="body">
    <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : '');?>">
      <!-- Header -->
      <div class="header">
        <div class="header-inner clearfix">
          <a class="brand pull-left" href="<?php echo $this->baseurl; ?>">
            <?php echo $logo;?> <?php if ($this->params->get('sitedescription')) { echo '<div class="site-description">'. htmlspecialchars($this->params->get('sitedescription')) .'</div>'; } ?>

                    </a>
                    <jdoc:include type="modules" name="header-overlay" style="headeroverlay" />
          <div class="header-search pull-right">
            <jdoc:include type="modules" name="search" style="none" />
          </div>
        </div>
      </div>
            <div class="horizontal-border">
                <jdoc:include type="modules" name="horizontal-border" style="none" />
            </div>
      <?php if ($this->countModules('breadcrumbs')) : ?>
      <div class="navigation">
        <jdoc:include type="modules" name="breadcrumbs" style="none" />
      </div>
      <?php endif; ?>
      <jdoc:include type="modules" name="banner" style="xhtml" />
      <div class="row-fluid">
        <?php if ($this->countModules('left-sidebar')) : ?>
        <!-- Begin Sidebar -->
        <div id="sidebar" class="span3">
                    <div class="left-container">

                        <div class="left-leaves">
                            <jdoc:include type="modules" name="left-leaves" style="none" />
                        </div>

                        <div class="sidebar-nav">
                          <jdoc:include type="modules" name="left-sidebar" style="well" />

                            <div class="left-leaves-bottom">
                                <jdoc:include type="modules" name="left-leaves-bottom" style="none" />
                            </div>
                        </div>


                  </div>
              </div>
        <!-- End Sidebar -->
        <?php endif; ?>
        <div id="content" class="<?php echo $span;?>">
          <!-- Begin Content -->
          <jdoc:include type="modules" name="main-title" style="well" />
          <jdoc:include type="message" />


          <jdoc:include type="component" />
          <jdoc:include type="modules" name="main" style="none" />

                    <!-- End Content -->
        </div>
        <?php if ($this->countModules('right-sidebar')) : ?>
        <div id="aside" class="span3">
                    <div class="right-container">
          <!-- Begin Right Sidebar -->
                    <div id="right-sidebar-toppusher">
             <jdoc:include type="modules" name="right-sidebar" style="well" />
                    </div>

                    <div class="right-sidebar-bottom">
                        <div id="pusherbox" class="pusherbox"></div>

                        <div id="bottomRightImage" class="main-bottom-right">
                            <jdoc:include type="modules" name="right-sidebar-bottom" style="none" />
                        </div>

                    </div>
          <!-- End Right Sidebar -->
                </div>
        </div>
        <?php endif; ?>
        <div class="leftbottom bottomtext">
          <p>&copy; <?php echo $sitename, " 2010-", date('Y');?></p>
        </div>
      </div>

      <div class="bottomtext">
        <p>Created by <a href="mailto:socapex@evolonline.org">Socapex</a>, <a href="mailto:reid@evolonline.org">Reid</a> and <a href="http://fey.ultra-book.com/">Alexandre Le Corre</a> for Evol Online.</p>
      </div>
    </div>

  </div>
<?php /* *** Footer removed ***
  <!-- Footer -->
  <div class="footer">
    <div class="container<?php echo ($params->get('fluidContainer') ? '-fluid' : '');?>">
      <hr />
      <jdoc:include type="modules" name="footer" style="none" />
      <p> Created by <a href="mailto:socapex@evolonline.org">Socapex</a>, <a href="mailto:reid@evolonline.org">Reid</a> and <a href="http://fey.ultra-book.com/">Alexandre Le Corre</a> for Evol Online.</p>
      <p class="pull-right"><a href="#top" id="back-top"><?php echo JText::_('TPL_PROTOSTAR_BACKTOTOP'); ?></a></p>
      <p>&copy; <?php echo $sitename, " 2010-", date('Y');?></p>
    </div>
  </div>
*/ ?>
  <jdoc:include type="modules" name="debug" style="none" />
</body>
</html>
